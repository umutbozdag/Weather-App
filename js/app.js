var searchBtn = document.getElementById("search-btn").addEventListener("click", showWeather);
var cityNameInput = document.getElementById("city-name-input");
var weatherIcon = document.querySelector(".weather-icon");
var tempValue = document.querySelector(".temperature-value p");
var tempDesc = document.querySelector(".temperature-description p");
var cityName = document.querySelector("#city-name p");
var notification = document.querySelector(".notification");
var api;

function showWeather() {

    if (cityNameInput.value == "") {
        alert("Please fill the input");
    }

    api = `http://api.openweathermap.org/data/2.5/weather?q=${cityNameInput.value}&lang=en&appid=e792e9d1044edc47e53443e8c6756cdd&units=metric`;

    console.log(api);

    fetch(api).then(function (response) {
        // The API call was successful!
        return response.json();
    }).then(function (data) {

        // This is the JSON from our response
        console.log(data.weather[0].icon);

        // Weather icon
        weatherIcon.innerHTML = `<img src="icons/${data.weather[0].icon}.png">`;
        // Temperature value
        tempValue.innerHTML = `${data.main.temp}°<span>C</span>`;
        // Temperature description
        tempDesc.innerHTML = data.weather[0].description;
        // City name
        cityName.innerHTML = `${data.name}`;

        notification.style.display = "none";

    }).catch(function (err) {
        // There was an error
        console.warn('Something went wrong.', err);

        notification.style.display = "block";
        notification.innerHTML = `<p>Something went wrong.</p>`;
    });
}