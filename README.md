# Weather-App
A weather app that written with vanilla javascript

# Technology
HTML, CSS, Javascript, Weather API from https://openweathermap.org/api

# Live Demo
Check it from here: https://umutbozdag.github.io/Weather-App/

Note: You have to give permission from top right corner of the browser to unblock content to see the weather results. 
